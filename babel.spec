%bcond_with bootstrap

Name: babel
Version: 2.15.0
Release: 1
Summary: Tools for internationalizing and localizing Python applications
License: BSD
URL: http://babel.pocoo.org/
Source0: https://github.com/python-babel/babel/releases/download/v%{version}/babel-%{version}.tar.gz

BuildArch: noarch

BuildRequires: gcc make
BuildRequires: python3-devel python3-setuptools
%if !%{with bootstrap}
BuildRequires: python3-pytz python3-pytest python3-freezegun python3-sphinx
%endif

Requires: python3-babel python3-setuptools

%description
Babel is an integrated collection of utilities that assist in internationalizing and
localizing Python applications, with an emphasis on web-based applications.

%package -n python3-babel
Summary: Library for internationalizing Python applications
Requires: python3-setuptools
Requires: python3-pytz

%description -n python3-babel
Babel is an integrated collection of utilities that assist in internationalizing and
localizing Python applications, with an emphasis on web-based applications.

%if !%{with bootstrap}
%package help
Summary:        Documentation for Babel
Provides:       python-babel-doc = %{version}-%{release}
Provides:       python3-babel-doc = %{version}-%{release}
Provides:       babel-doc
Obsoletes:      babel-doc < %{version}-%{release}

%description help
Documentation for Babel
%endif

%prep
%autosetup -n babel-%{version} -p1

%build
%py3_build

BUILDDIR="$PWD/built-docs"
rm -rf "$BUILDDIR"

%if !%{with bootstrap}
pushd docs
make \
    SPHINXBUILD=sphinx-build-3 \
    BUILDDIR="$BUILDDIR" \
    html
popd
rm -f "$BUILDDIR/html/.buildinfo"
%endif

%install
%py3_install

%check
export TZ=UTC
%if !%{with bootstrap}
%{__python3} -m pytest
%endif

%pre

%preun

%post

%postun

%files
%doc CHANGES.rst AUTHORS
%license LICENSE
%{_bindir}/pybabel

%files -n python3-babel
%{python3_sitelib}/Babel-%{version}-py*.egg-info
%{python3_sitelib}/babel

%if !%{with bootstrap}
%files help
%doc built-docs/html/*
%endif

%changelog
* Thu Jul 11 2024 dillon chen <dillon.chen@gmail.com> - 2.15.0-1
- update to 2.15.0 

* Mon Aug 7 2023 zhoupengcheng <zhoupengcheng11@huawei.com> - 2.12.1-1
- update to 2.12.1 
- backport two patches from upstream to fix testcase failure after upgrade 2.12.1
    backport-Freeze-format_time-tests-to-a-specific-date-to-fix-t.patch
    backport-Use-aware-UTC-datetimes-internally.patch

* Thu Jan 19 2023 zhangnan <zhangnan134@huawei.com> - 2.11.0-1
- update to 2.11.0

* Sat Oct 22 2022 yanglongkang <yanglongkang@h-partners.com> - 2.10.3-2
- fix rpmbuild warning

* Mon Jul 4 2022 panxiaohe <panxh.life@foxmail.com> - 2.10.3-1
- update to 2.10.3

* Tue Feb 15 2022 yangzhuangzhuang <yangzhuangzhuang1@h-partners.com> - 2.9.1-4
- enable make check

* Fri Dec 3 2021 panxiaohe <panxiaohe@huawei.com> - 2.9.1-3
- add Bootstrap for Python 3.10

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.9.1-2
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Fri Jul 30 2021 panxiaohe <panxiaohe@huawei.com> - 2.9.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 2.9.1

* Tue May 11 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 2.9.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix CVE-2021-20095

* Sat Jan 23 2021 zoulin <zoulin13@huawei.com> - 2.9.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 2.9.0

* Thu Oct 29 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 2.8.0-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove python2

* Mon Jun 29 2020 Liquor <lirui130@huawei.com> - 2.8.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 2.8.0

* Wed Jun 24 2020 chenditang<chenditang1@huawei.com> - 2.7.0-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:fix tests when using Python 3.9a6

* Fri Oct 11 2019 hanzhijun<hanzhijun1@huawei.com> - 2.7.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 2.7.0

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 2.6.0-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.6.0-6
- Package init

